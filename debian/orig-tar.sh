#!/bin/sh -e

VERSION=$2
DOWNLOADED_FILE=$3
PACKAGE=$(dpkg-parsechangelog | sed -n 's/^Source: //p')
TAR=../${PACKAGE}_${VERSION}+dfsg1.orig.tar.gz
DIR=${PACKAGE}-${VERSION}

svn export svn://svn.freehep.org/svn/freehep/tags/$PACKAGE-$VERSION $DIR
rm -f $DIR/src/main/java/org/freehep/xml/util/XMLCharacterProperties.java
rm -f $DIR/src/main/resources/org/freehep/xml/util/dtd/*
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR

rm -f $DOWNLOADED_FILE

